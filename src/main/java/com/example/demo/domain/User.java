package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * @program:demo3
 * @ClassName User
 * @description:
 * @author: qianqizheng
 * @Version 1.0
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class User {
        /**
         *用户ID
         */
        @NotBlank
        private Integer id;
        /**
         *用户姓名
         * */
        @NotBlank
        private String name;
        /**
         *用户性别
         * */
        @NotBlank
        private String gender ;
        /**
         *用户生日
         * */
        @NotBlank
        private String birthday;

}
