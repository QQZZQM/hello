package com.example.demo.Controller;


import com.example.demo.domain.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program:demo3
 * @ClassName UserController
 * @description:
 * @author: qianqizheng
 * @Version 1.0
 **/
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/find")
    public User find(Integer id) {
        User userr = userService.findUser(id);
        return userr;
    }

    @RequestMapping("/add")
    public String add(User user) {
        userService.insertUser(user);
        return "已新增";
    }
    @RequestMapping("/update")
    public String update(User user){
        userService.updateUser(user);
        return "已修改";
    }
    @RequestMapping("/delete")
    public String delete(Integer id) {
         userService.deleteUser(id);
         return "ok";


    }

}
