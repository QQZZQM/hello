package com.example.demo.mapper;

import com.example.demo.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UserMapper {

    User selectById(@Param("id") Integer id);


    int inserInfo(@Param("us") User user);

    int update(@Param("user") User user);

    User deleteById(@Param("idd") Integer id);
}
