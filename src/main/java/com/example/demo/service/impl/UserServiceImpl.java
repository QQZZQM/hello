package com.example.demo.service.impl;

import com.example.demo.domain.User;
import com.example.demo.mapper.UserMapper;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program:demo3
 * @ClassName UserServiceImpl
 * @description:
 * @author: qianqizheng
 * @Version 1.0
 **/

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findUser(Integer id) {
        User user = userMapper.selectById(id);
        return user;

    }

    @Override
    public int insertUser(User user) {

            int res = userMapper.inserInfo(user);
            return res;

    }

    @Override
    public int updateUser(User user) {
        int ress = userMapper.update(user);
        return ress;

    }

    @Override
    public User deleteUser(Integer id) {
       User ree = userMapper.deleteById(id);
        return ree;

    }


}
