package com.example.demo.service;

import com.example.demo.domain.User;

public interface UserService {

    public User findUser(Integer id);

    public int insertUser(User user);

    public int updateUser(User user);

    public User deleteUser(Integer id);
}
